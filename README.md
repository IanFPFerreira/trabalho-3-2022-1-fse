# Trabalho 3 - 2022-1 - FSE

Trabalho final da disciplina de Fundamentos de Sistemas Embarcados, tendo comoo objetivo manipular sensores e atuadores distribuídos baseados nos microcontroladores ESP32 interconectados via Wifi através do protocolo MQTT. Uma descrição mais detalhada do trabalho pode ser encontrada [aqui](https://gitlab.com/fse_fga/trabalhos-2022_1/trabalho-3-2022-1).

## Integrantes

| Nome | Matrícula |
| :--- | :---: |
| Ian Fillipe Pontes Ferreira | 18/0102087 |
| Ítalo Fernandes Sales de Serra | 18/0102613 |

## Link da Apresentação

[vídeo](https://youtu.be/lENJh3DXAC8)

## Bibliotecas

- cJSON.h
- 0nism/ESP32-DHT11@^1.0.1

## Como executar

- **Passo 1:** Instale o [PlatformIO](https://platformio.org/install/ide?install=vscode) no Visual Studio Code.

- **Passo 2:** Clone este repositório.

- **Passo 3:** Abrir o projeto no PlatformIO, sendo que o projeto deve ser aberto pela pasta "ESP32" que se encontra neste repositório, pois é a pasta que contém o arquivo "platformio.ini", que é o arquivo que reconhecerá o projeto como um projeto do PlatformIO. 

- **Passo 4:** Conectar o ESP32 ao Wifi.
    - Para isso, utilize o comando no terminal do PlatformIO: 
        ```bash
        pio run -t menuconfig
        ```
    [![menuconfig](img/config.png)](img/config.png)

    Na tela acima, vá até a opção "Configuração do Wifi Configuration" e configure o nome e a senha do Wifi.
    
- **Passo 5:** Realizar o build do projeto.
    - Para isso, clique no botão 'PlatformIO: Build' ou utilize o comando no terminal do PlatformIO: 
        ```bash
        pio run
        ```
- **Passo 6:** Realizar o upload do projeto.
    - Para isso, clique no botão 'PlatformIO: Upload' ou utilize o comando no terminal do PlatformIO: 
        ```bash
        pio run -t upload
        ```
- **Passo 7:** Abrir o monitor serial do projeto.
    - Para isso, clique no botão 'PlatformIO: Serial Monitor' ou utilize o comando no terminal do PlatformIO: 
        ```bash
        pio device monitor
        ```
- **Passo 8:** Abrir o projeto no ThingsBoard na Dashboard 'Ian-Italo'.

## Sensores utilizados

### Sensor DHT11

<img src="img/dht11.png" alt="drawing" width="350"/>

Sensor utilizador para leitura da umidade e temperatura ambiente.

### Led da ESP32

Controlamos o led da própria ESP32.


## Dashboard

[![menuconfig](img/dashboard.png)](img/dashboard.png)

O site utilizado foi o ThingsBoard para execução dos testes, onde mostramos a temperatura e umidade através da telemetria e escolhemos a intensidade do led da ESP32 por meio de um PWM.
